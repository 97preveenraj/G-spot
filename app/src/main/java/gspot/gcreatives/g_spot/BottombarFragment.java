package gspot.gcreatives.g_spot;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


public class BottombarFragment extends Fragment implements View.OnClickListener{

    ImageButton feedbtn;
    ImageButton accountbtn;
    TextView Feedtext;
    TextView Accounttext;

    Communicator communicator;

    Boolean accountbuttonflag;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        communicator = (Communicator) activity;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.bottom_bar,container,false);

        feedbtn = (ImageButton) view.findViewById(R.id.FeedButton);
        accountbtn = (ImageButton) view.findViewById(R.id.AccountButton);

        Feedtext = (TextView) view.findViewById(R.id.FeedText);
        Accounttext = (TextView) view.findViewById(R.id.AccountText);

        accountbuttonflag=false;


        feedbtn.setOnClickListener(this);
        accountbtn.setOnClickListener(this);




        return view;
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.FeedButton){


            accountbuttonflag=false;

            Accounttext.setTextColor(getResources().getColor(R.color.colorBlack));
            Feedtext.setTextColor(getResources().getColor(R.color.colorAccent));

            accountbtn.setImageResource(R.drawable.account_black);
            feedbtn.setImageResource(R.drawable.feed_white);
           // Toast.makeText(getActivity(),"Feed Button clicked",Toast.LENGTH_SHORT).show();

            communicator.accountkiller();

        }
        else
            if (view.getId() == R.id.AccountButton){


                if(!accountbuttonflag) {

                    accountbuttonflag=true;
                    Feedtext.setTextColor(getResources().getColor(R.color.colorBlack));
                    Accounttext.setTextColor(getResources().getColor(R.color.colorAccent));

                    feedbtn.setImageResource(R.drawable.feed_black);
                    accountbtn.setImageResource(R.drawable.account_white);
                    // Toast.makeText(getActivity(),"Account Button clicked",Toast.LENGTH_SHORT).show();

                    communicator.ShowAccountfrag();

                }

            }



    }

    public void killme(){
        getActivity().getFragmentManager().beginTransaction().remove(this).commit();
    }
}
