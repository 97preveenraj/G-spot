package gspot.gcreatives.g_spot;

import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.support.v4.content.res.ResourcesCompat;



/**
 * Created by Key Smash on 17-Jun-17.
 */

public class Login_page extends DialogFragment implements  View.OnClickListener {
    private EditText uid, password;
    private Button submit;
    private String tempuid, temppass;
    private int i;
    private boolean flag;

    private String[] db_uid;
    private String[] db_name;
    private String[] db_pswrd;
    private  String Username;

    int backcount=0;


    Communicator communicator;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(getActivity(), getTheme()){

            @Override
            public void onBackPressed() {
                if(backcount==0) {
                    Toast.makeText(getActivity(), "Press back again to exit!", Toast.LENGTH_SHORT).show();
                    backcount++;
                }
                else
                    communicator.killer();
            }
        };
    }






    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        communicator = (Communicator) activity;
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.login_layout, null);

        db_uid = getResources().getStringArray(R.array.res_uid);
        db_name = getResources().getStringArray(R.array.res_name);
        db_pswrd = getResources().getStringArray(R.array.res_pswrd);


        submit = (Button) view.findViewById(R.id.submit);
        uid = (EditText) view.findViewById(R.id.uid);
        password = (EditText) view.findViewById(R.id.password);
        submit.setOnClickListener(this);
        setCancelable(false);
        return view;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.submit) {
            tempuid = uid.getText().toString().toUpperCase();
            temppass = password.getText().toString().toUpperCase();
            flag=false;
            String msg;
            for (i = 0; i < db_uid.length; i++) {
                Log.i("vila(db_uid)", db_uid[i]);
                Log.i("vila(db_pswrd)", db_pswrd[i]);

                Log.i("vila(et_uid)", tempuid);
                Log.i("vila(et_pswrd)", temppass);

                if (tempuid.equals(db_uid[i])) {

                    if (temppass.equals(db_pswrd[i])) {
                        flag=true;
                        communicator.flagconnector(flag);
                        Username = db_name[i];
                        communicator.nameconnector(Username);
                        break;
                    }
                }
            }
            if (flag == true) {
                msg = "Hey " + db_name[i];
                dismiss();
                communicator.notify_inflator();
            } else {
                msg = "Try again!";
            }
            Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
        }


    }






}


